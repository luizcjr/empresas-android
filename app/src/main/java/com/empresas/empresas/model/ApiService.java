package com.empresas.empresas.model;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiService {

    @POST("users/auth/sign_in")
    @Headers("Content-Type: application/json")
    Call<Login> postLogin(@Body Login login);
}
