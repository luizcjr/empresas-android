package com.empresas.empresas.util;

public class ItensEmpresas {

    private int mImagemEmpresa;
    private String nomeEmpresa;
    private String negocioEmpresa;
    private String paisEmpresa;

    public ItensEmpresas(int mImagemEmpresa, String nomeEmpresa, String negocioEmpresa, String paisEmpresa) {
        this.mImagemEmpresa = mImagemEmpresa;
        this.nomeEmpresa = nomeEmpresa;
        this.negocioEmpresa = negocioEmpresa;
        this.paisEmpresa = paisEmpresa;
    }

    public int getmImagemEmpresa() {
        return mImagemEmpresa;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public String getNegocioEmpresa() {
        return negocioEmpresa;
    }

    public String getPaisEmpresa() {
        return paisEmpresa;
    }
}
