package com.empresas.empresas.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.empresas.empresas.R;
import com.empresas.empresas.activities.EmpresaActivity;
import com.empresas.empresas.util.ItensEmpresas;

import java.util.ArrayList;

public class EmpresasAdapter extends RecyclerView.Adapter<EmpresasAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<ItensEmpresas> mLista;

    public EmpresasAdapter(Context mContext, ArrayList<ItensEmpresas> mLista) {
        this.mContext = mContext;
        this.mLista = mLista;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_empresas, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ItensEmpresas itemAtual = mLista.get(position);

        holder.txtNomeEmpresa.setText(itemAtual.getNomeEmpresa());
        holder.txtNegocioEmpresa.setText(itemAtual.getNegocioEmpresa());
        holder.txtPaisEmpresa.setText(itemAtual.getPaisEmpresa());

        holder.constraintBusca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detalheEmpresa = new Intent(mContext, EmpresaActivity.class);
                detalheEmpresa.putExtra("nomeEmpresa", itemAtual.getNomeEmpresa());
                mContext.startActivity(detalheEmpresa);
            }
        });

        Glide
                .with(mContext)
                .load(itemAtual.getmImagemEmpresa())
                .into(holder.ivLogoEmpresas);
    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }

    public void filtarLista(ArrayList<ItensEmpresas> listaFiltrada) {
        mLista = listaFiltrada;
        notifyDataSetChanged();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtNomeEmpresa, txtNegocioEmpresa, txtPaisEmpresa;
        public ImageView ivLogoEmpresas;
        public ConstraintLayout constraintBusca;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtNomeEmpresa = itemView.findViewById(R.id.txtNomeEmpresa);
            txtNegocioEmpresa = itemView.findViewById(R.id.txtNegocioEmpresa);
            txtPaisEmpresa = itemView.findViewById(R.id.txtPaisEmpresa);
            ivLogoEmpresas = itemView.findViewById(R.id.ivLogoEmpresas);
            constraintBusca = itemView.findViewById(R.id.constraintBusca);
        }
    }
}
