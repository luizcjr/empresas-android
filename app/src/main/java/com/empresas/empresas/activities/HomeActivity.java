package com.empresas.empresas.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.empresas.empresas.R;
import com.empresas.empresas.adapter.EmpresasAdapter;
import com.empresas.empresas.util.ItensEmpresas;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private ConstraintLayout constraintLogo, constraintSearchView;
    private ImageView ivSearch, ivCloseSearchView, ivSearchView;
    private EditText edtSearch;
    private RecyclerView recyclerEmpresas;
    private ArrayList<ItensEmpresas> mList;
    private TextView txtLabelBusca;
    EmpresasAdapter empresasAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        constraintLogo = findViewById(R.id.constraintLogo);
        constraintSearchView = findViewById(R.id.constraintSearchView);
        ivSearch = findViewById(R.id.ivSearch);
        ivCloseSearchView = findViewById(R.id.ivCloseSearchView);
        ivSearchView = findViewById(R.id.ivSearchView);
        edtSearch = findViewById(R.id.edtSearch);
        txtLabelBusca = findViewById(R.id.txtLabelBusca);
        recyclerEmpresas = findViewById(R.id.recyclerEmpresas);

        criarLista();

        empresasAdapter = new EmpresasAdapter(this, mList);
        recyclerEmpresas.setAdapter(empresasAdapter);
        recyclerEmpresas.setHasFixedSize(true);

        RecyclerView.LayoutManager empresasLayout = new LinearLayoutManager(this);
        recyclerEmpresas.setLayoutManager(empresasLayout);

        //Listener para verificar o que está sendo digitado para a procura na recyclerview
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(edtSearch.getText().toString().isEmpty()) {
                    recyclerEmpresas.setVisibility(View.GONE);
                } else {
                    filtro(s.toString());
                    recyclerEmpresas.setVisibility(View.VISIBLE);
                }
            }
        });

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Testando as visibilidades dos componentes que tem que ser mostrados para o usuário, caso o botão de busca seja acionado
                if (constraintLogo.getVisibility() == View.VISIBLE) {
                    constraintLogo.setVisibility(View.GONE);
                    txtLabelBusca.setVisibility(View.GONE);
                    constraintSearchView.setVisibility(View.VISIBLE);
                } else {
                    esconderTeclado(HomeActivity.this);
                    constraintLogo.setVisibility(View.VISIBLE);
                    txtLabelBusca.setVisibility(View.VISIBLE);
                    recyclerEmpresas.setVisibility(View.GONE);
                    constraintSearchView.setVisibility(View.GONE);
                }
            }
        });

        ivCloseSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Testando as visibilidades dos componentes que tem que ser mostrados para o usuário, caso o botão de sair da busca seja acionado
                if (constraintSearchView.getVisibility() == View.VISIBLE) {
                    esconderTeclado(HomeActivity.this);
                    constraintSearchView.setVisibility(View.GONE);
                    constraintLogo.setVisibility(View.VISIBLE);
                    txtLabelBusca.setVisibility(View.VISIBLE);
                    edtSearch.getText().clear();
                } else {
                    constraintSearchView.setVisibility(View.VISIBLE);
                    constraintLogo.setVisibility(View.GONE);
                    txtLabelBusca.setVisibility(View.GONE);
                }
            }
        });
    }

    //Metodo para percorrer os itens da lista, de acordo com as letras digitadas
    private void filtro(String texto) {
        ArrayList<ItensEmpresas> lista = new ArrayList<>();

        for (ItensEmpresas item : mList) {
            if (item.getNomeEmpresa().toLowerCase().contains(texto.toLowerCase())) {
                lista.add(item);
            }
        }
        empresasAdapter.filtarLista(lista);
    }

    //Metodo para criação da lista, alimentando a recyclerview
    private void criarLista() {
        mList = new ArrayList<>();
        mList.add(new ItensEmpresas(R.drawable.img_e_1_lista, "Empresa1", "Negócio", "Brasil"));
    }

    //Metodo para esconder o teclado, quando ele não será mais ncessário
    public void esconderTeclado(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();

        if(view == null) {
            view = new View(activity);
        }

        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    //Metodo para verificar se o usuário deseja sair do aplicativo, caso aperte o botão back do celular
    @Override
    public void onBackPressed() {

        final AlertDialog.Builder dialog;
        //Instanciando o dialog
        dialog = new AlertDialog.Builder(HomeActivity.this, R.style.DialogStyle);

        //Criando título do alerta
        dialog.setTitle("Sair?");

        //Mensagem
        dialog.setMessage("Deseja realmente sair?");

        //Não fechar o alerta clicando em qualquer lugar
        dialog.setCancelable(false);

        //Configurar o botao não
        //Dois parâmetros a mensagem e o que ser feito quando for clicado
        dialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        //Configurar botão positivo
        dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        dialog.create();
        dialog.show();
    }
}
