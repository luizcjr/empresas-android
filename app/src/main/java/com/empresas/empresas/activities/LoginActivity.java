package com.empresas.empresas.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.empresas.empresas.R;
import com.empresas.empresas.model.ApiService;
import com.empresas.empresas.model.Cliente;
import com.empresas.empresas.model.Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText edtEmail, edtSenha;
    private Button btnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtEmail = findViewById(R.id.edtEmail);
        edtSenha = findViewById(R.id.edtSenha);
        btnEntrar = findViewById(R.id.btnEntrar);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Pegando os valores digitados pelo usuário
                String email = edtEmail.getText().toString();
                String password = edtSenha.getText().toString();

                Login login = new Login(email, password);
                ApiService apiService = Cliente.retrofit.create(ApiService.class);

                //Fazendo chamada da api com o metodo Post de Login
                Call<Login> call = apiService.postLogin(login);
                call.enqueue(new Callback<Login>() {
                    @Override
                    public void onResponse(Call<Login> call, Response<Login> response) {
                        if(response.isSuccessful()) {
                            Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(homeIntent);
                            finish();
                        } else {
                            Toast.makeText(getBaseContext(), "Usuário ou senha incorretos!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Login> call, Throwable t) {
                        Toast.makeText(LoginActivity.this, "Falha de conexão!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
}
