package com.empresas.empresas.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.empresas.empresas.R;

public class EmpresaActivity extends AppCompatActivity {

    private ImageView ivVoltar, ivLogoEmpresas;
    private TextView txtNomeEmpresaToolbar, txtDescricao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa);

        ivVoltar = findViewById(R.id.ivVoltar);
        ivLogoEmpresas = findViewById(R.id.ivLogoEmpresas);
        txtNomeEmpresaToolbar = findViewById(R.id.txtNomeEmpresaToolbar);
        txtDescricao = findViewById(R.id.txtDescricao);

        //Metodo para voltar no último estado da pilha, caso a imagem de voltar seja acionada
        ivVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmpresaActivity.super.onBackPressed();
            }
        });

        //Recebendo valores do adapter, contendo o nome para mostrar na toolbar
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String nomeEmpresa = extras.getString("nomeEmpresa");

            txtNomeEmpresaToolbar.setText(nomeEmpresa);
        }

        //Instanciando a imagem pela biblioteca glide
        Glide
                .with(EmpresaActivity.this)
                .load(R.drawable.img_e_1)
                .into(ivLogoEmpresas);
    }

    //Metodo para voltar no último estado da pilha, caso o botão back do celular seja acionado
    @Override
    public void onBackPressed() {
        EmpresaActivity.super.onBackPressed();
    }
}
